import os
import pytest


@pytest.fixture(scope="session")
def hub(hub):
    try:
        if os.uname().sysname.upper().endswith("BSD"):
            return hub
    except:
        ...

    pytest.skip("idem-bsd is only intended for BSD systems")
